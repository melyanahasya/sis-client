import { BrowserRouter, Route, Switch } from "react-router-dom";
import AlokasiGuru from "./Akademik/AlokasiGuru";
import DataSiswa from "./Akademik/DataSiswa";
import Guru from "./Akademik/Guru";
import MutasiSiswa from "./Akademik/MutasiSiswa";
import PembagianKelas from "./Akademik/PembagianKelas";
import PendaftaranSiswa from "./Akademik/PendaftaranSiswa";
import SeleksiSiswa from "./Akademik/SeleksiSiswa";
import "./App.css";
import Dashboard from "./Component/Dashboard";
import Navbar from "./Component/Navbar";
import Sidebar from "./Component/Sidebar";
import JenisMataPelajaran from "./Page/JenisMataPelajaran";
import Jenjang from "./Page/Jenjang";
import Kelas from "./Page/Kelas";
import MataPelajaran from "./Page/MataPelajaran";
import RombonganBelajar from "./Page/RombonganBelajar";
// import TahunAjaran from "./Page/TahunAjaran";

function App() {
  return (
    <div>
      <Navbar/>
      <BrowserRouter>
        <main className="flex md:gap-x-0 gap-x-[4.7rem]">
          <div>
            <Sidebar/>
          </div>
          <Switch>
            <Route path="/" component={Dashboard} exact />
            {/* <Route path="/tahunajaran" component={TahunAjaran} exact /> */}
            <Route path="/jenjang" component={Jenjang} exact />
            <Route path="/kelas" component={Kelas} exact />
            <Route path="/alokasiguru" component={AlokasiGuru} exact />
            <Route path="/seleksiSiswa" component={SeleksiSiswa} exact />
            <Route path="/pembagiankelas" component={PembagianKelas} exact />
            <Route path="/datasiswa" component={DataSiswa} exact />
            <Route path="/mutasisiswa" component={MutasiSiswa} exact />
            <Route path="/pendaftaransiswa" component={PendaftaranSiswa} exact />
            <Route
              path="/rombonganBelajar"
              component={RombonganBelajar}
              exact
            />
            <Route
              path="/jenisMataPelajaran"
              component={JenisMataPelajaran}
              exact
            />
            <Route path="/mataPelajaran" component={MataPelajaran} exact />
            
            <Route path="/guru" component={Guru} exact />
          </Switch>
        </main>
      </BrowserRouter>
    </div>
  );
}

export default App;
