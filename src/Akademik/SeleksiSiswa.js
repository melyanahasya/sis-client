import React from "react";
import "../Css/Sidebar.css"

export default function SeleksiSiswa() {
  return (
    <div>
      <div>
        <div class="container">
          <div className="flex justify-between">
            <h2 class="text-2xl font-bold leading-tight mt-5">
              Seleksi dan Pembagian Siswa
            </h2>
            <p className="mt-4 text-gray-500">
              Dashboard <i class="fa-solid fa-angles-right"></i> Seleksi Siswa
            </p>
          </div>
          <div class="py-3 border border-t-4 border-t-gray-500 p-6 mt-7  shadow-lg flex justify-between gap-x-[10rem]">
            <div>
              <div className="flex gap-x-[47rem]">
                <h2 class="text-base text-gray-500 leading-tight font-medium">
                  Seleksi Siswa
                </h2>
              </div>
              <div className="flex">
                <p className="text-lg mt-4 border-b-2 w-[30rem] text-gray-800">
                  Data Pendaftaran
                </p>
                <hr className="h-10" />
              </div>
              <div>
                <div class="mb-4 ml-[16.6rem] flex mt-5 gap-x-3">
                  <label
                    class="block text-grey-darker text-sm font-bold mt-3"
                    for="search"
                  >
                    Search
                  </label>
                  <input
                    class="shadow appearance-none border rounded w-[10rem] py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline"
                    id="username"
                    type="text"
                  />
                </div>
              </div>
              <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 w-fit ">
                <div class="inline-block w-[30rem]  sidebar overflow-x-auto -mt-3 ">
                  <table class="leading-normal w-fit border">
                    <thead className="text-center border-b">
                      <tr>
                        <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider">
                          No
                        </th>
                        <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider">
                          No Reg
                        </th>
                        <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider">
                          Jenjang
                        </th>
                        <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider">
                          Aksi
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                          <p class="text-gray-900 whitespace-no-wrap">1</p>
                        </td>
                        <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                          <p class="text-gray-900 whitespace-no-wrap">
                            46545455
                          </p>
                        </td>
                        <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                          <p class="text-gray-900 whitespace-no-wrap">test</p>
                        </td>

                        <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm flex justify-around gap-3">
                          <button class="rounded-lg px-4 py-2  bg-blue-600 text-green-100 duration-300">
                          <i class="fas fa-search-plus"></i>
                          </button>
                          <button class="rounded-lg px-4 py-2  bg-green-600 text-green-100 duration-300">
                          <i class="fas fa-share"></i>
                          </button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <div class="bg-white ml-[14rem]  flex items-center flex-wrap">
                <nav aria-label="Page navigation">
                  <ul class="inline-flex">
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 rounded-l-lg focus:shadow-outline hover:bg-green-100">
                        Prev
                      </button>
                    </li>
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline">
                        1
                      </button>
                    </li>
                    <li>
                      <button class="px-4 py-2 text-white transition-colors duration-150 bg-blue-600 border border-r-0 border-blue-600 focus:shadow-outline">
                        2
                      </button>
                    </li>
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline hover:bg-green-100">
                        3
                      </button>
                    </li>
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-blue-600 rounded-r-lg focus:shadow-outline hover:bg-green-100">
                        Next
                      </button>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>

            <div className=" w-fit mt-5">
              <div className="flex w-fit justify-between gap-x-[3.2rem]">
                <p className="text-lg mt-4 border-b-2 w-[29.6rem] text-gray-800">
                  Data Seleksi
                </p>
                <hr className="h-10" />
              </div>
              <div class="mb-4 ml-[16.6rem] flex mt-5 gap-x-3 w-fit">
                <label
                  class="block text-grey-darker text-sm font-bold mt-3"
                  for="search"
                >
                  Search
                </label>
                <input
                  class="shadow appearance-none border rounded w-[10rem] py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline"
                  id="username"
                  type="text"
                />
              </div>
              <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 w-fit ">
                <div class="inline-block w-[30rem]  sidebar overflow-x-auto -mt-3">
                  <table class="leading-normal w-fit border">
                    <thead className="text-center ">
                      <tr>
                        <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider">
                          No
                        </th>
                        <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider">
                          No Reg
                        </th>
                        <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider">
                          Jenjang
                        </th>
                        <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider">
                          Aksi
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                          <p class="text-gray-900 whitespace-no-wrap">1</p>
                        </td>
                        <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                          <p class="text-gray-900 whitespace-no-wrap">
                            46545455
                          </p>
                        </td>
                        <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                          <p class="text-gray-900 whitespace-no-wrap">test</p>
                        </td>

                        <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm flex justify-around gap-3">
                          <button class="rounded-lg px-4 py-2  bg-blue-600 text-green-100 duration-300">
                          <i class="fas fa-search-plus"></i>
                          </button>
                          <button class="rounded-lg px-4 py-2  bg-green-600 text-green-100 duration-300">
                          <i class="fas fa-share"></i>
                          </button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <div class=" p-4 flex items-center ml-[12.5rem] -mt-3  w-fit">
                <nav aria-label="Page navigation">
                  <ul class="inline-flex">
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 rounded-l-lg focus:shadow-outline hover:bg-green-100">
                        Prev
                      </button>
                    </li>
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline">
                        1
                      </button>
                    </li>
                    <li>
                      <button class="px-4 py-2 text-white transition-colors duration-150 bg-blue-600 border border-r-0 border-blue-600 focus:shadow-outline">
                        2
                      </button>
                    </li>
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline hover:bg-green-100">
                        3
                      </button>
                    </li>
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-blue-600 rounded-r-lg focus:shadow-outline hover:bg-green-100">
                        Next
                      </button>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
