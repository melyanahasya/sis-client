import React from "react";

export default function PembagianKelas() {
  return (
    <div>
      <div class="container ml-[1rem]">
        <div className="flex justify-between">
          <h2 class="text-2xl font-bold leading-tight mt-5">
            {" "}
            Seleksi dan Pembagian Kelas
          </h2>
          <p className="mt-5">Dashboard / Seleksi Siswa / Pembagian Kelas</p>
        </div>
        <div class="py-3 border border-t-4 border-t-gray-500 p-6 mt-7 w-[73rem] shadow-lg">
          <div className="flex gap-x-[48.5rem]">
            <h2 class="text-xl text-gray-500 leading-tight font-medium">
              Pembagian Kelas
            </h2>
            <button class="rounded-lg px-4 py-2 ml-[5.8rem]  bg-sky-500 text-green-100 duration-300">
              Jenjang
            </button>
          </div>
          <div className="mt-5 flex gap-x-4">
            <input
              class="shadow appearance-none border rounded w-[14rem] py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline"
              id="username"
              type="text"
              placeholder="Pilih Rombel"
            />
            <button class="rounded-lg px-4 py-2  bg-blue-500 text-green-100 duration-300">
              Simpan
            </button>
          </div>
          <div>
            <div class="mb-4 ml-[50.4rem] flex mt-5 gap-x-3">
              <label
                class="block text-grey-darker text-sm font-bold mt-3"
                for="search"
              >
                Search
              </label>
              <input
                class="shadow appearance-none border rounded w-[16rem] py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline"
                id="username"
                type="text"
              />
            </div>
          </div>
          <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 w-[70rem] ">
            <div class="inline-block shadow-md w-[70rem] -mt-3 overflow-x-auto sidebar">
              <table class="leading-normal border w-[70rem] ">
                <thead className="text-center ">
                  <tr>
                    <th
                      scope="col"
                      className="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider"
                    >
                      <div class="custom-control custom-checkbox">
                        <input
                          type="checkbox"
                          class="custom-control-input"
                          id="customCheck2"
                        />
                      </div>
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider">
                      No_Req
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider">
                      Tahun Ajaran
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider">
                      Jenjang
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider">
                      Nama
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-xs  text-gray-700 text-center  tracking-wider">
                      Jekel
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-xs  text-gray-700 text-center  tracking-wider">
                      Tempat Lahir
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-xs  text-gray-700 text-center  tracking-wider">
                      Tanggal Lahir
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td
                      scope="col"
                      className="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm"
                    >
                      <div class="custom-control custom-checkbox">
                        <input
                          type="checkbox"
                          class="custom-control-input"
                          id="customCheck2"
                        />
                      </div>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">046545455</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">
                        TA2020/2023
                      </p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">SMK</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">Test</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <span class="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
                        <span
                          aria-hidden
                          class="absolute inset-0 bg-green-200 opacity-50 rounded-full"
                        ></span>
                        <span class="relative">L</span>
                      </span>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">Semarang</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">1999-</p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <div class="bg-white p-4 flex items-center flex-wrap ml-[53rem]">
            <nav aria-label="Page navigation">
              <ul class="inline-flex">
                <li>
                  <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 rounded-l-lg focus:shadow-outline hover:bg-green-100">
                    Prev
                  </button>
                </li>
                <li>
                  <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline">
                    1
                  </button>
                </li>
                <li>
                  <button class="px-4 py-2 text-white transition-colors duration-150 bg-blue-600 border border-r-0 border-blue-600 focus:shadow-outline">
                    2
                  </button>
                </li>
                <li>
                  <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline hover:bg-green-100">
                    3
                  </button>
                </li>
                <li>
                  <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-blue-600 rounded-r-lg focus:shadow-outline hover:bg-green-100">
                    Next
                  </button>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  );
}
