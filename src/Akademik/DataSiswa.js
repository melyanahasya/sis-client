import React from "react";

export default function DataSiswa() {
  return (
    <div class="container ml-[1rem]">
      <div className="flex gap-x-[54rem]">
        <h2 class="text-2xl font-bold leading-tight mt-5"> Data Siswa</h2>
        <p className="mt-5">Dashboard / Data Siswa</p>
      </div>
      <div class="py-3 border border-t-4 border-t-gray-500 p-6 mt-7 w-[73rem] shadow-lg">
        <div className="flex gap-x-[43.4rem]">
          <h2 class="text-xl text-gray-500 leading-tight font-medium">
            Data Siswa
          </h2>
          <div className="flex justify-around gap-x-4">
            <button class="rounded-lg px-4 py-2  bg-green-500 hover:bg-green-600 text-green-100 duration-300">
              <i class="fas fa-download"></i> Export Data
            </button>
            <button class="rounded-lg px-4 py-2  bg-green-500 hover:bg-green-600 text-green-100 duration-300">
              <i class="fas fa-search-plus"></i> Tampilkan Data
            </button>
          </div>
        </div>
        <div>
          <div class="mb-4 ml-[50.4rem] flex mt-5 gap-x-3">
            <label
              class="block text-grey-darker text-sm font-bold mt-3"
              for="search"
            >
              Search
            </label>
            <input
              class="shadow appearance-none border rounded w-[16rem] py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline"
              id="username"
              type="text"
            />
          </div>
        </div>
        <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 w-[70rem] ">
          <div class="inline-block shadow-md w-[70rem] -mt-3 overflow-x-auto sidebar">
            <table class="leading-normal border w-[70rem] ">
              <thead className="text-center ">
                <tr>
                  <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider">
                    No
                  </th>
                  <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider">
                    Rombel
                  </th>
                  <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider">
                    NISN
                  </th>
                  <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider">
                    Nama
                  </th>
                  <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700  tracking-wider">
                    Jekel
                  </th>
                  <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-xs  text-gray-700 text-center  tracking-wider">
                    Tempat Lahir
                  </th>
                  <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-xs  text-gray-700 text-center  tracking-wider">
                    Tanggal Lahir
                  </th>
                  <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-xs  text-gray-700 text-center  tracking-wider">
                    Alamat
                  </th>
                  <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-xs  text-gray-700 text-center  tracking-wider">
                    Aksi
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                    <p class="text-gray-900 whitespace-no-wrap">1</p>
                  </td>
                  <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                    <p class="text-gray-900 whitespace-no-wrap">XI TKJ 2 </p>
                  </td>
                  <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                    <p class="text-gray-900 whitespace-no-wrap">5465412054</p>
                  </td>
                  <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                    <p class="text-gray-900 whitespace-no-wrap">
                      Petter Sulaiman
                    </p>
                  </td>
                  <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                    <span class="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
                      <span
                        aria-hidden
                        class="absolute inset-0 bg-green-200 opacity-50 rounded-full"
                      ></span>
                      <span class="relative">L</span>
                    </span>
                  </td>
                  <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                    <p class="text-gray-900 whitespace-no-wrap">Semarang</p>
                  </td>
                  <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                    <p class="text-gray-900 whitespace-no-wrap">1999-06-18</p>
                  </td>
                  <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                    <p class="text-gray-900 whitespace-no-wrap">JAwa Tengah</p>
                  </td>
                  <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                    <div className="flex justify-around gap-x-[1rem]">
                        <div className="flex">
                    <button class="rounded-lg px-4 py-2  bg-blue-500 hover:bg-blue-500 text-green-100 duration-300">
                      <i class="fas fa-download"></i> Edit
                    </button>
                    </div>
                    <button class="rounded-lg px-4 py-2  bg-orange-500 hover:bg-orange-600 text-green-100 duration-300">
                      <i class="fas fa-search-plus"></i> Detail
                    </button>
                    <button class="rounded-lg px-4 py-2  bg-red-500 hover:bg-red-600 text-green-100 duration-300">
                    <i class="fas fa-trash-alt"></i> Delete
                    </button>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div class="bg-white p-4 flex items-center flex-wrap ml-[53rem]">
          <nav aria-label="Page navigation">
            <ul class="inline-flex">
              <li>
                <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 rounded-l-lg focus:shadow-outline hover:bg-green-100">
                  Prev
                </button>
              </li>
              <li>
                <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline">
                  1
                </button>
              </li>
              <li>
                <button class="px-4 py-2 text-white transition-colors duration-150 bg-blue-600 border border-r-0 border-blue-600 focus:shadow-outline">
                  2
                </button>
              </li>
              <li>
                <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline hover:bg-green-100">
                  3
                </button>
              </li>
              <li>
                <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-blue-600 rounded-r-lg focus:shadow-outline hover:bg-green-100">
                  Next
                </button>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  );
}
