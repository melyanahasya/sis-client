import React, { useState } from "react";
import { Modal } from "react-bootstrap";

export default function Guru() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <div>
      <div class=" container mx-auto">
        <h2 class="text-2xl font-bold leading-tight mt-5"> Guru</h2>
        <div class="py-3 border border-t-4 border-t-gray-500 p-6 mt-7 w-[73rem] shadow-lg">
          <div className="flex gap-x-[58.4rem]">
            <h2 class="text-xl text-gray-500 leading-tight font-medium">Data Guru</h2>
            <button onClick={handleShow} class="rounded-lg px-4 py-2 bg-green-700 text-green-100 hover:bg-green-800 duration-300">
              Tambah
            </button>
          </div>
          <div>
            <div class="mb-4 ml-[50.4rem] flex mt-5 gap-x-3">
              <label
                class="block text-grey-darker text-sm font-bold mt-3"
                for="search"
              >
                Search 
              </label>
              <input
                class="shadow appearance-none border rounded w-[16rem] py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline"
                id="username"
                type="text"
              />
            </div>
          </div>
          <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 w-[74rem] overflow-x-auto">
            <div class="inline-block  shadow-md overflow-hidden w-full -mt-3">
              <table class="leading-normal border w-[70rem]">
                <thead className="text-center ">
                  <tr>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                      No
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                      NIP
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                      Nama
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                      Jekel
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                      Alamat
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-xs  text-gray-700 text-center uppercase tracking-wider">
                      Aksi
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p>1</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">46545455</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">test</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <span class="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
                        <span
                          aria-hidden
                          class="absolute inset-0 bg-green-200 opacity-50 rounded-full"
                        ></span>
                        <span class="relative">L</span>
                      </span>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">Test</p>
                    </td>
                    <td class="px-12 py-5 border-gray-200 bg-gray-200 text-sm flex justify-center gap-x-4">
                     <a href="/alokasiguru"> <button  class="rounded-lg px-4 py-2 bg-green-700 text-green-100 hover:bg-green-800 duration-300">
                        Alokasi
                      </button></a>
                      <button class="rounded-lg px-4 py-2 bg-blue-500 text-blue-100 hover:bg-blue-600 duration-300">
                        Edit
                      </button>
                      <button class="rounded-lg px-4 py-2 bg-red-600 text-red-100 hover:bg-red-700 duration-300">
                        Hapus
                      </button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <div class="bg-white p-4 flex items-center flex-wrap ml-[53rem]">
            <nav aria-label="Page navigation">
              <ul class="inline-flex">
                <li>
                  <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 rounded-l-lg focus:shadow-outline hover:bg-green-100">
                    Prev
                  </button>
                </li>
                <li>
                  <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline">
                    1
                  </button>
                </li>
                <li>
                  <button class="px-4 py-2 text-white transition-colors duration-150 bg-blue-600 border border-r-0 border-blue-600 focus:shadow-outline">
                    2
                  </button>
                </li>
                <li>
                  <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline hover:bg-green-100">
                    3
                  </button>
                </li>
                <li>
                  <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-blue-600 rounded-r-lg focus:shadow-outline hover:bg-green-100">
                    Next
                  </button>
                </li>
              </ul>
            </nav>
          </div>

          <Modal
                show={show}
                onHide={handleClose}
                id="authentication-modal"
                tabindex="-1"
                aria-hidden="true"
                className="md:ml-[30%] ml-0 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
              >
                <div className="relative w-full h-full max-w-md md:h-auto">
                  <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
                    <button
                      type="button"
                      className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                      data-modal-hide="authentication-modal"
                      onClick={handleClose}
                    >
                      <svg
                        aria-hidden="true"
                        className="w-5 h-5"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fill-rule="evenodd"
                          d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                          clip-rule="evenodd"
                        ></path>
                      </svg>
                      <span className="sr-only">Close modal</span>
                    </button>
                    <div className="px-6 py-6 lg:px-8">
                      <h3 className="mb-4 text-xl font-medium text-gray-900 dark:text-white">
                        Tambahkan
                      </h3>
                      <form
                        className="space-y-3"
                        // onSubmit={add}
                      >
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                           Nip
                          </label>
                          <input
                            placeholder=" Nip"
                            // onChange={(e) => setNip(e.target.value)}
                            // value={nip}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Nama
                          </label>
                          <input
                            placeholder="Nama"
                            // onChange={(e) => setNama(e.target.value)}
                            // value={nama}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Jekel
                          </label>
                          <input
                            placeholder=" Jekel"
                            // onChange={(e) => setJekel(e.target.value)}
                            // value={jekel}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Alamat
                          </label>
                          <input
                            placeholder=" Alamat"
                            // onChange={(e) => setAlamat(e.target.value)}
                            // value={alamat}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <button
                          onClick={handleClose}
                          type="submit"
                          className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                        >
                          Tambah
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
              </Modal>
        </div>
      </div>
    </div>
  );
}
