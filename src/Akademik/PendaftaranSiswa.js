import React from "react";
import { useState } from "react";
import { Modal } from "react-bootstrap";
import "../Css/Sidebar.css";

export default function PendaftaranSiswa() {
  const [show, setShow] = useState(false);
  const [upload, setUpload] = useState(false);

  const handleClose = () => setShow(false);
  const handleClose1 = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <div>
      <div class="container -ml-[1rem]">
        <div className="flex justify-between">
          <h2 class="text-2xl font-bold leading-tight mt-5">
            {" "}
            Pendaftaran Siswa
          </h2>
          <p className="mt-5">Dashboard / Pendaftaran</p>
        </div>
        <div class="py-3 border border-t-4 border-t-gray-500 p-6 mt-7 w-[73rem] shadow-lg">
          <h2 class="text-xl text-gray-500 leading-tight font-medium">
            Pendaftaran Siswa
          </h2>
          <div className="flex gap-x-5 justify-between mt-5">
            <div className="space-x-2">
            <button 
            onClick={() => setUpload(true)}
             class="rounded-lg px-4 py-2 bg-green-700 text-green-100 hover:bg-green-800 duration-300">
              <i class="fa-solid fa-cloud-arrow-up"></i> Upload
            </button>
            <button onClick={handleShow} class=" rounded-lg px-4 py-2 bg-green-700 text-green-100 hover:bg-green-800 duration-300">
              Tambah
            </button>
            </div>
            <select
              id="countries"
              class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg float-right focus:ring-blue-500 focus:border-blue-500 block w-[10rem] md:p-1.5 p-1 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            >
              <option selected>Filter</option>
              <option value="aa">aa</option>
              <option value="aa">aa</option>
              <option value="aa">aa</option>
            </select>
          </div>
          <div>
            <div class="mb-4 ml-[50.4rem] flex mt-5 gap-x-3">
              <label
                class="block text-grey-darker text-sm font-bold mt-3"
                for="search"
              >
                Search
              </label>
              <input
                class="shadow appearance-none border rounded w-[16rem] py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline"
                id="username"
                type="text"
              />
            </div>
          </div>
          <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 w-[70rem] ">
            <div class="inline-block shadow-md w-[70rem] -mt-3 overflow-x-auto sidebar">
              <table class="leading-normal border w-[70rem] ">
                <thead className="text-center ">
                  <tr>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                      No
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                      No_Req
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                      Tahun Ajaran
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                      Jenjang
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                      Nama
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-xs  text-gray-700 text-center uppercase tracking-wider">
                      Jekel
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-xs  text-gray-700 text-center uppercase tracking-wider">
                      Tempat Lahir
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-xs  text-gray-700 text-center uppercase tracking-wider">
                      Tanggal Lahir
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-xs  text-gray-700 text-center uppercase tracking-wider">
                      Aksi
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p>1</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">046545455</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">TA2020/2023</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">SMK</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">Test</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <span class="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
                        <span
                          aria-hidden
                          class="absolute inset-0 bg-green-200 opacity-50 rounded-full"
                        ></span>
                        <span class="relative">L</span>
                      </span>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">Semarang</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">1999-</p>
                    </td>
                    <td class="px-12 py-5 border-gray-200 bg-gray-200 text-sm flex justify-center gap-x-4">
                      <button class="rounded-lg px-4 py-2 bg-orange-500 text-green-100 hover:bg-green-800 duration-300">
                      <i class="fa-sharp fa-solid fa-magnifying-glass"></i>
                      </button>
                      <button class="rounded-lg px-4 py-2 bg-blue-500 text-blue-100 hover:bg-blue-600 duration-300">
                      <i class="fa-solid fa-pen-to-square"></i>
                      </button>
                      <button class="rounded-lg px-4 py-2 bg-red-600 text-red-100 hover:bg-red-700 duration-300">
                      <i class="fa-solid fa-trash"></i>
                      </button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <div class="bg-white p-4 flex items-center flex-wrap ml-[53rem]">
            <nav aria-label="Page navigation">
              <ul class="inline-flex">
                <li>
                  <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 rounded-l-lg focus:shadow-outline hover:bg-green-100">
                    Prev
                  </button>
                </li>
                <li>
                  <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline">
                    1
                  </button>
                </li>
                <li>
                  <button class="px-4 py-2 text-white transition-colors duration-150 bg-blue-600 border border-r-0 border-blue-600 focus:shadow-outline">
                    2
                  </button>
                </li>
                <li>
                  <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline hover:bg-green-100">
                    3
                  </button>
                </li>
                <li>
                  <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-blue-600 rounded-r-lg focus:shadow-outline hover:bg-green-100">
                    Next
                  </button>
                </li>
              </ul>
            </nav>
          </div>

         {/* modal add */}
          <Modal
                show={show}
                onHide={handleClose}
                id="authentication-modal"
                tabindex="-1"
                aria-hidden="true"
                className="md:ml-[30%] ml-0 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
              >
                <div className="relative w-full h-full max-w-md md:h-auto">
                  <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
                    <button
                      type="button"
                      className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                      data-modal-hide="authentication-modal"
                      onClick={handleClose}
                    >
                      <svg
                        aria-hidden="true"
                        className="w-5 h-5"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fill-rule="evenodd"
                          d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                          clip-rule="evenodd"
                        ></path>
                      </svg>
                      <span className="sr-only">Close modal</span>
                    </button>
                    <div className="px-6 py-6 lg:px-8">
                      <h3 className="mb-4 text-xl font-medium text-gray-900 dark:text-white">
                        Tambahkan
                      </h3>
                      <form
                        className="space-y-3"
                        // onSubmit={add}
                      >
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            No Req
                          </label>
                          <input
                            placeholder="No Req"
                            // onChange={(e) => setNoReq(e.target.value)}
                            // value={noReq}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Tahun Ajaran
                          </label>
                          <input
                            placeholder="Tahun Ajaran"
                            // onChange={(e) => setTahunAjaran(e.target.value)}
                            // value={tahunAjaran}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Jenjang
                          </label>
                          <input
                            placeholder="Jenjang"
                            // onChange={(e) => setJenjang(e.target.value)}
                            // value={jenjang}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Nama
                          </label>
                          <input
                            placeholder=" Nama"
                            // onChange={(e) => setNama(e.target.value)}
                            // value={nama}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Jekel
                          </label>
                          <input
                            placeholder=" Jekel"
                            // onChange={(e) => setJekel(e.target.value)}
                            // value={jekel}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Tempat Lahir
                          </label>
                          <input
                            placeholder=" Tempat Lahir"
                            // onChange={(e) => setTempatLahir(e.target.value)}
                            // value={tempatLahir}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Tanggal Lahir
                          </label>
                          <input
                            placeholder=" Tanggal Lahir"
                            // onChange={(e) => setTanggalLahir(e.target.value)}
                            // value={tanggalLahir}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <button
                          onClick={handleClose}
                          type="submit"
                          className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                        >
                          Tambah
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
              </Modal>

              {/* modal upload */}
              <Modal
                show={upload}
                onHide={!upload}
                id="authentication-modal"
                tabindex="-1"
                aria-hidden="true"
                className="md:ml-[30%] ml-0 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
              >
                <div className="relative w-full h-full max-w-md md:h-auto">
                  <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
                    <button
                      type="button"
                      className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                      data-modal-hide="authentication-modal"
                      onClick={() => setUpload(false)}
                    >
                      <svg
                        aria-hidden="true"
                        className="w-5 h-5"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fill-rule="evenodd"
                          d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                          clip-rule="evenodd"
                        ></path>
                      </svg>
                      <span className="sr-only">Close modal</span>
                    </button>
                    <div className="px-6 py-6 lg:px-8">
                      <h3 className="mb-4 text-xl font-medium text-gray-900 dark:text-white">
                        Tambahkan
                      </h3>
                      <form
                        className="space-y-3"
                        // onSubmit={add}
                      >
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Nama Kelas
                          </label>
                          <input
                            placeholder="Nama Kelas"
                            // onChange={(e) => setNamaKelas(e.target.value)}
                            // value={namaKelas}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Jenjang
                          </label>
                          <input
                            placeholder="Jenjang"
                            // onChange={(e) => setJenjang(e.target.value)}
                            // value={jenjang}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Keterangan
                          </label>
                          <input
                            placeholder=" Keterangan"
                            // onChange={(e) => setKeterangan(e.target.value)}
                            // value={keterangan}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <button
                          onClick={handleClose1}
                          type="submit"
                          className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                        >
                          Tambah
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
              </Modal>
        </div>
      </div>
    </div>
  );
}
