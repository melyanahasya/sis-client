import React from "react";
import "../Css/Sidebar.css"

export default function MutasiSiswa() {
  return (
    <div class="container mx-auto">
      <div className="flex gap-x-[50.7rem]">
        <h2 class="text-2xl font-bold leading-tight mt-5">Mutasi Siswa</h2>
        <p className="mt-6 text-gray-500 ">
          Dashboard / Mutasi Siswa
        </p>
      </div>
      <div class="py-3 border w-fit border-t-4 border-t-gray-500 p-6 mt-7 shadow-lg flex gap-x-[7rem]">
        <div className="">
          <div className="flex gap-x-[1rem]">
            <h2 class="text-baseleading-tight font-medium mt-1">
             Pilih Kelas :
            </h2>
            <select
              id="countries"
              class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg float-right focus:ring-blue-500 focus:border-blue-500 block w-[15rem] md:p-1.5 p-1 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 "
            >
              <option selected>Pilih Kelas</option>
              <option value="">X TB</option>
              <option value="">X TBSM </option>
              <option value="">X Akutansi</option>
              <option value="">X TKJ 1</option>
              <option value="">X TKJ 2</option>
              <option value="">XI TB </option>
              <option value="">XI TBSM </option>
              <option value="">XI Akutansi</option>
              <option value="">XI TKJ 1</option>
              <option value="">XI TKJ 2 </option>
              <option value="">XII TB</option>
              <option value="">XII TBSM</option>
              <option value="">XII Akutansi</option>
              <option value="">XII TKJ 1</option>
              <option value="">XII TKJ 2</option>
            </select>
          </div>
          <div>
            <div class="mb-4 ml-[26.4rem] flex mt-5 gap-x-3">
              <label
                class="block text-grey-darker text-sm font-bold mt-3"
                for="search"
              >
                Search
              </label>
              <input
                class="shadow appearance-none border rounded w-[10rem] py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline"
                id="username"
                type="text"
              />
            </div>
          </div>
          <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4  w-fit">
            <div class="inline-block  shadow-md overflow-x-auto w-[40rem] sidebar -mt-3">
              <table class="leading-normal border w-[30rem]">
                <thead className="text-center ">
                  <tr>
                    <th
                      scope="col"
                      className="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider"
                    >
                      <div class="custom-control custom-checkbox">
                        <input
                          type="checkbox"
                          class="custom-control-input"
                          id="customCheck2"
                        />
                      </div>
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                     Rombel
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                      NISN
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                      Nama
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                      Jekel
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                      TTL
                    </th>
                    <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                      Alamat    
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td
                      scope="col"
                      className="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm"
                    >
                      <div class="custom-control custom-checkbox">
                        <input
                          type="checkbox"
                          class="custom-control-input"
                          id="customCheck2"
                        />
                      </div>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">XI TKJ 2</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">16476414035</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">PeterSuleiman</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">L</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">Semarang/1999-06-13</p>
                    </td>
                    <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                      <p class="text-gray-900 whitespace-no-wrap">Jawa Barat</p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <div class="bg-white ml-[23.7rem]  flex items-center flex-wrap">
            <nav aria-label="Page navigation">
              <ul class="inline-flex">
                <li>
                  <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 rounded-l-lg focus:shadow-outline hover:bg-green-100">
                    Prev
                  </button>
                </li>
                <li>
                  <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline">
                    1
                  </button>
                </li>
                <li>
                  <button class="px-4 py-2 text-white transition-colors duration-150 bg-blue-600 border border-r-0 border-blue-600 focus:shadow-outline">
                    2
                  </button>
                </li>
                <li>
                  <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline hover:bg-green-100">
                    3
                  </button>
                </li>
                <li>
                  <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-blue-600 rounded-r-lg focus:shadow-outline hover:bg-green-100">
                    Next
                  </button>
                </li>
              </ul>
            </nav>
          </div>
        </div>

        <div className=" w-fit mt-5 ">
          <div className="flex w-fit justify-between gap-x-[3.2rem]">
            <p className="text-lg mt-4 border-b-2 w-[19rem] text-gray-800">
             Mutasi Siswa
            </p>
            <hr className="h-10" />
          </div>
            <div className="flex gap-x-[1rem] mt-4">
               <p> Jenis :</p>
               <select
              id="countries"
              class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg float-right focus:ring-blue-500 focus:border-blue-500 block w-[15rem] md:p-1.5 p-1 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 "
            >
              <option selected>Pilih</option>
              <option value="naik kelas">Naik Kelas</option>
              <option value="pindah kelas">Pindah Kelas</option>
              <option value="pindah sekolah">Pindah Sekolah</option>
              <option value="lulus">Lulus</option>
            </select>
            </div>
        </div> 
      </div>
    </div>
  );
}
