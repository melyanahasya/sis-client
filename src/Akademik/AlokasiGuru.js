import React from "react";

export default function AlokasiGuru() {
  return (
    <div>
      <div>
        <div class="container mx-auto">
          <div className="flex justify-between">
            <h2 class="text-2xl font-bold leading-tight mt-5">Alokasi Guru</h2>
            <p className="mt-4 text-gray-500">
              Dashboard <i class="fa-solid fa-angles-right"></i> Mapel{" "}
              <i class="fa-solid fa-angles-right"></i> Alokasi Mapel
            </p>
          </div>
          <div class="py-3 border border-t-4 border-t-gray-500 p-6 mt-7 shadow-lg flex justify-between gap-x-[10rem]">
            <div className="">
              <div className="flex gap-x-[47rem]">
                <h2 class="text-base text-gray-500 leading-tight font-medium">
                  Alokasi Mata Pelajaran{" "}
                  <i class="fa-sharp fa-solid fa-arrow-right"></i> Guru
                </h2>
              </div>
              <div className="flex justify-around">
                <p className="text-lg mt-4 border-b-2 w-[19rem] text-gray-800">
                  Data Mata Pelajaran
                </p>
                <hr className="h-10" />
                <button class="rounded-lg px-4 py-2 ml-[5.8rem] mt-4  bg-green-700 text-green-100 hover:bg-green-800 duration-300">
                  Simpan
                </button>
              </div>
              <div>
                <div class="mb-4 ml-[16.6rem] flex mt-5 gap-x-3">
                  <label
                    class="block text-grey-darker text-sm font-bold mt-3"
                    for="search"
                  >
                    Search
                  </label>
                  <input
                    class="shadow appearance-none border rounded w-[10rem] py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline"
                    id="username"
                    type="text"
                  />
                </div>
              </div>
              <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto w-fit">
                <div class="inline-block  shadow-md overflow-hidden w-[30rem] -mt-3">
                  <table class="leading-normal border w-[30rem]">
                    <thead className="text-center ">
                      <tr>
                        <th
                          scope="col"
                          className="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider"
                        >
                          <div class="custom-control custom-checkbox">
                            <input
                              type="checkbox"
                              class="custom-control-input"
                              id="customCheck2"
                            />
                          </div>
                        </th>
                        <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                          Nama Mapel
                        </th>
                        <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                          Keterangan
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td
                          scope="col"
                          className="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm"
                        >
                          <div class="custom-control custom-checkbox">
                            <input
                              type="checkbox"
                              class="custom-control-input"
                              id="customCheck2"
                            />
                          </div>
                        </td>
                        <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                          <p class="text-gray-900 whitespace-no-wrap">
                            46545455
                          </p>
                        </td>
                        <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                          <p class="text-gray-900 whitespace-no-wrap">test</p>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <div class="bg-white  flex items-center flex-wrap">
                <nav aria-label="Page navigation">
                  <ul class="inline-flex">
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 rounded-l-lg focus:shadow-outline hover:bg-green-100">
                        Prev
                      </button>
                    </li>
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline">
                        1
                      </button>
                    </li>
                    <li>
                      <button class="px-4 py-2 text-white transition-colors duration-150 bg-blue-600 border border-r-0 border-blue-600 focus:shadow-outline">
                        2
                      </button>
                    </li>
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline hover:bg-green-100">
                        3
                      </button>
                    </li>
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-blue-600 rounded-r-lg focus:shadow-outline hover:bg-green-100">
                        Next
                      </button>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>

            <div className=" w-fit mt-5">
              <div className="flex w-fit justify-between gap-x-[3.2rem]">
                <p className="text-lg mt-4 border-b-2 w-[19rem] text-gray-800">
                  Data Alokasi Ke Kelas
                </p>
                <hr className="h-10" />
                <button class="rounded-lg px-4 py-2 mt-4  bg-red-700 text-green-100 hover:bg-green-800 duration-300">
                  Hapus
                </button>
              </div>
              <div class="mb-4 ml-[16.6rem] flex mt-5 gap-x-3 w-fit">
                <label
                  class="block text-grey-darker text-sm font-bold mt-3"
                  for="search"
                >
                  Search
                </label>
                <input
                  class="shadow appearance-none border rounded w-[10rem] py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline"
                  id="username"
                  type="text"
                />
              </div>
              <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 w-fit overflow-x-auto">
                <div class="inline-block  shadow-md overflow-hidden w-fit -mt-3">
                  <table class="leading-normal w-fit">
                    <thead className="text-center ">
                      <tr>
                        <th
                          scope="col"
                          className="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider"
                        >
                          <div class="custom-control custom-checkbox">
                            <input
                              type="checkbox"
                              class="custom-control-input"
                              id="customCheck2"
                            />
                          </div>
                        </th>
                        <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                          Nama Mapel
                        </th>
                        <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                          Keterangan
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td
                          scope="col"
                          className="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm"
                        >
                          <div class="custom-control custom-checkbox">
                            <input
                              type="checkbox"
                              class="custom-control-input"
                              id="customCheck2"
                            />
                          </div>
                        </td>
                        <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                          <p class="text-gray-900 whitespace-no-wrap">
                            46545455
                          </p>
                        </td>
                        <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                          <p class="text-gray-900 whitespace-no-wrap">test</p>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <div class=" p-4 flex items-center ml-[12.5rem] -mt-3  w-fit">
                <nav aria-label="Page navigation">
                  <ul class="inline-flex">
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 rounded-l-lg focus:shadow-outline hover:bg-green-100">
                        Prev
                      </button>
                    </li>
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline">
                        1
                      </button>
                    </li>
                    <li>
                      <button class="px-4 py-2 text-white transition-colors duration-150 bg-blue-600 border border-r-0 border-blue-600 focus:shadow-outline">
                        2
                      </button>
                    </li>
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 focus:shadow-outline hover:bg-green-100">
                        3
                      </button>
                    </li>
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-blue-600 rounded-r-lg focus:shadow-outline hover:bg-green-100">
                        Next
                      </button>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
