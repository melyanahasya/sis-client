import React from "react";
import { useState } from "react";
import "../Css/Sidebar.css";

export default function Dashboard() {
  const slides = [
    {
      url: "https://storage.googleapis.com/narasi-production.appspot.com/production/medium/1671785416672/5-rekomendasi-buku-pengembangan-diri-yang-wajib-dibaca-medium.webp",
    },
    {
      url: "https://media.istockphoto.com/id/1003251564/id/vektor/perpustakaan-kartun.jpg?s=170667a&w=0&k=20&c=qshInFVuicwnNqgI2kwpr9wFUhFm4bbuj_BT53wkMTY=",
    },
    {
      url: "https://1.bp.blogspot.com/-vTXqTyjG3t8/X5BEspjuBHI/AAAAAAAAA8M/mgUF7tb6nPEW8Y1g3e_3x09uKEvX6sOGQCLcBGAsYHQ/s531/statistik.jpg",
    },
    {
      url: "https://gurunda.com/wp-content/uploads/2021/12/memberi-pr-anak-TK.png",
    },
    {
      url: "https://imgix2.ruangguru.com/assets/miscellaneous/png_hditl0_460.png",
    },
    {
      url: "https://dilmil-jayapura.go.id/wp-content/uploads/2022/01/keuangan.jpg",
    },
  ];

  const [currentIndex, setCurrentIndex] = useState(0);

  const prevSlide = () => {
    const isFirstSlide = currentIndex === 0;
    const newIndex = isFirstSlide ? slides.length - 1 : currentIndex - 1;
    setCurrentIndex(newIndex);
  };

  const nextSlide = () => {
    const isLastSlide = currentIndex === slides.length - 1;
    const newIndex = isLastSlide ? 0 : currentIndex + 1;
    setCurrentIndex(newIndex);
  };
  return (
    <div>
      <div className="py-7 font-semibold flex-1 w-[80rem] h-[90vh] overflow-y-auto overflow-x-hidden">
        <div>
          <div className="flex gap-2 ml-3 md:ml-2 mt-2 md:mt-4">
            <p className="md:text-2xl text-xl text-left">Dashboard</p>
            <p className="mt-1 text-sm md:text-lg text-gray-500">
              Halaman Utama
            </p>
          </div>
          <div className="md:flex block gap-5">
            <div className=" flex gap-[8rem] md:gap-[66%] border w-fit md:w-[30%] md:ml-2 mt-8 p-3 h-16 border-t-green-600 border-t-4">
              <p className="text-lg">Akademik</p>
              <i class="fas fa-plus mt-1 text-lg"></i>
            </div>
            <div className=" flex gap-[6rem] md:gap-[50%] border w-fit md:w-[30%] md:ml-2 mt-8 p-3 h-16 border-t-green-600 border-t-4">
              <p className="text-lg">Perpustakaan</p>
              <i class="fas fa-plus mt-1 text-lg"></i>
            </div>
            <div className=" flex gap-[7.5rem] md:gap-[60%] border w-fit md:w-[30%] md:ml-2 mt-8 p-3 h-16 border-t-green-600 border-t-4">
              <p className="text-lg">Nilai Siswa</p>
              <i class="fas fa-plus mt-1 text-lg"></i>
            </div>
          </div>

          <div className=" flex gap-[8rem] md:gap-[66%] border w-fit md:w-[30%] md:ml-2 mt-8 p-3 h-16 border-t-green-600 border-t-4">
            <p className="text-lg">Keuangan</p>
            <i class="fas fa-plus mt-1 text-lg"></i>
          </div>

          <div className="block md:flex gap-5">
            <div className="mt-8 ml-[1px] md:ml-2 bg-blue-500 text-white w-[16rem] md:w-[30%] md:h-[10rem] h-[8rem] rounded-t-xl">
              <div className="">
                <div className="flex gap-[6rem] md:gap-[6rem]">
                  <div>
                    <p className="text-lg md:text-2xl font-normal md:font-bold ml-4 md:ml-4 pt-8 -mt-2 md:-mt-3">
                      0
                    </p>
                    <p className="ml-4 md:ml-4 text-sm md:text-xl">
                      Jumlah Siswa
                    </p>
                  </div>
                  <i class="fas fa-user-alt mt-9 text-4xl md:text-5xl"></i>
                </div>
                <p className="mt-9 md:mt-12 text-center bg-blue-600 text-sm md:text-lg">
                  Detail
                </p>
              </div>
            </div>
            <div className="mt-8 ml-[1px] md:ml-2 bg-green-500 text-white w-[16rem] md:w-[30%] md:h-[10rem] h-[8rem] rounded-t-xl">
              <div className="">
                <div className="flex gap-7 md:gap-[3rem]">
                  <div>
                    <p className="text-lg md:text-2xl font-normal md:font-bold ml-4 md:ml-4 -mt-2 md:-mt-2 pt-8">
                      0
                    </p>
                    <p className=" ml-4 md:ml-4 w-[9rem] md:w-[13rem] text-sm md:text-lg">
                      Jumlah Calon Siswa Baru
                    </p>
                  </div>
                  <i class="fas fa-user-plus mt-9 text-4xl md:text-5xl"></i>
                </div>
                <p className="mt-4 md:mt-6 text-center bg-green-600 text-sm md:text-lg">
                  Detail
                </p>
              </div>
            </div>
            <div className="mt-8 ml-[1px] md:ml-2 bg-orange-500 text-white w-[16rem] md:w-[30%]  md:h-[10rem] h-[7.5rem] rounded-t-xl">
              <div className="">
                <div className="flex gap-3 md:gap-[2rem]">
                  <div>
                    <p className="text-lg md:text-2xl font-normal md:font-bold ml-4 md:ml-4 pt-8 -mt-2 md:-mt-2">
                      0
                    </p>
                    <p className=" ml-4 md:ml-4 w-[10rem] md:w-[13rem] text-sm md:text-lg">
                      Jumlah Peminjaman Hari Ini
                    </p>
                  </div>
                  {/* <i class="fad fa-book mt-5 text-7xl"></i> */}
                  {/* <i class="fal fa-book mt-5 "></i> */}
                  <i class="fas fa-address-book mt-7 md:mt-9 text-4xl md:text-7xl"></i>
                </div>
                <p className="mt-3 md:mt-6 text-center bg-orange-600 text-sm md:text-lg">
                  Detail
                </p>
              </div>
            </div>
          </div>

          <div className=" mt-8 ml-[1px] md:ml-2 bg-red-500 text-white w-[16rem] md:w-[30%] md:h-[10rem] h-[6.9rem] rounded-t-xl">
            <div className="">
              <div className="flex gap-16 md:gap-[7rem]">
                <div>
                  <p className="text-lg md:text-2xl font-normal md:font-bold ml-4 md:ml-4 pt-8 -mt-2 md:mt-4">
                    0
                  </p>
                  <p className="ml-4 md:ml-4 text-sm md:text-lg">
                    Jumlah Anggota
                  </p>
                </div>
                {/* <i class="fal fa-users-class "></i> */}
                <i class="fas fa-user-friends mt-7 md:mt-14 text-4xl md:text-5xl"></i>
              </div>
              <p className="mt-6 md:mt-12 text-center bg-red-600 text-sm md:text-lg">
                Detail
              </p>
            </div>
          </div>

          <div className="md:flex">
            <div>
            <div className="flex md:ml-3">
              <div className="bg-blue-500 p-4 md:p-4 w-fit mt-10">
                <i class="fas fa-user-friends text-white text-xl md:text-4xl"></i>
              </div>
              <div className="bg-gray-200 mt-10 w-[12rem] md:w-[19rem]">
                <p className="text-sm md:text-lg mt-3 ml-3 md:ml-2 font-normal md:font-semibold">
                  Masukan Email
                </p>
              </div>
            </div>
            <div className="flex -mt-7 md:ml-3">
              <div className="bg-green-500 p-4 md:p-4 w-fit mt-10">
                <i class="fas fa-user-friends text-white text-xl md:text-4xl"></i>
              </div>
              <div className="bg-gray-200 mt-10 w-[12rem] md:w-[19rem]">
                <p className="text-sm md:text-lg mt-3 ml-3 md:ml-2 font-normal md:font-semibold">
                  Masukan Email
                </p>
              </div>
            </div>
            </div>

            <div
              className="md:max-w-[44rem] md:h-[30rem] h-[20rem] max-w-[23rem] md:w-full m-auto
           py-16 px-4 relative group -mt-[14rem]"
            >
              <div
                style={{backgroundImage: `url(${slides[currentIndex].url})` }}
                className="w-full h-full rounded-2xl bg-center bg-cover duration-500 border-black border"
              ></div>
              <div className="hidden md:group-hover:block  group-hover:block absolute top-[45%] -translate-x-0 trslate-y-[-50%] -left-3 text-2xl rounded-full py-3 px-[1rem] bg-black/20 text-white cursor-pointer">
                <i
                  class="fa-solid fa-angles-left text-2xl"
                  onClick={prevSlide}
                ></i>
              </div>
              <div className="hidden md:group-hover:block group-hover:block absolute top-[45%] -translate-x-0 trslate-y-[-50%] -right-3 text-2xl rounded-full py-3 px-[1rem] bg-black/20 text-white cursor-pointer">
                <i class="fa-solid fa-angles-right text-2xl"onClick={nextSlide}></i>
              </div>
            </div>
          </div>

          <div className="mt-8 -mb-1 md:-mb-5 text-left md:mr-0  :text-center">
            <hr className="w-[15rem]"/>
            <p className="p-1 md:p-5 text-xs md:text-sm ">
              <span className="font-black text-gray-500">
                <i class="fas fa-copyright"></i> <span> CopyRigth 2023{" "}</span>
                <span className="text-blue-700"> FE</span>
              </span>{" "}
              <span className="text-gray-500">. All Rigths Reversed</span>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
