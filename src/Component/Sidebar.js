import React from "react";
import { useState } from "react";
import "../Css/Sidebar.css";

export default function Sidebar() {
  const [open, setOpen] = useState(false);
  return (
    <div
      class={`${
        open ? "w-[14rem] md:w-[14rem]" : "w-[20%] md:w-[40%]"
      } duration-300 flex h-screen md:h-full flex-col justify-between border-r bg-[#0A2647] overflow-x-auto overflow-y-auto sidebar m-0 md:static absolute`}
    >
      <div class="px-4 py-6">
        <span class="grid h-10 w-20 place-content-center rounded-lg text-xs text-white -ml-4">
          <i
            class={`fa-solid fa-list cursor-pointer duration-500 text-2xl rounded-lg rotate ${
              open && "rotate-[180deg]"
            }`}
            onClick={() => setOpen(!open)}
          ></i>
        </span>

        <nav aria-label="Main Nav" class="mt-6 flex flex-col space-y-1">
          {/* <div className="px-4">
          <input
            class={`shadow appearance-none border rounded py-1 px-3 w-36 text-grey-darker leading-tight focus:outline-none focus:shadow-outline ${!open && "scale-0"}`}
            id="username"
            type="text"
            placeholder="Search"
          />
          </div> */}
          <details class="group [&_summary::-webkit-details-marker]:hidden">
            <summary class="flex cursor-pointer items-center justify-between rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700">
              <div class="flex items-center gap-2">
              <i class="fas fa-chart-line"></i>

                <a
                  href="/"
                  className={`text-sm font-medium ${!open && "scale-0"}`}
                >
                  <span class={`text-sm font-medium`}> Dashboard </span>
                </a>
              </div>
            </summary>
          </details>

          <details class="group [&_summary::-webkit-details-marker]:hidden">
            <summary class="flex cursor-pointer items-center justify-between rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700">
              <div class="flex items-center gap-2">
                <i class="fa-solid fa-graduation-cap"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Akademik{" "}
                </span>
              </div>

              <span
                class={`shrink-0 transition duration-300 group-open:-rotate-180 ${
                  !open && "scale-0"
                }`}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fill-rule="evenodd"
                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                    clip-rule="evenodd"
                  />
                </svg>
              </span>
            </summary>

            <nav aria-label="Teams Nav" class="mt-2 flex flex-col px-4">
              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <i class="fa-solid fa-calendar-days"></i>
                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Tahun Ajaran{" "}
                </span>
              </a>

              <a
                href="/rombonganBelajar"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <i class="fa-solid fa-people-group"></i>
                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Rombongan Belajar{" "}
                </span>
              </a>

              <a
                href="/mataPelajaran"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <i class="fa-solid fa-book-atlas"></i>
                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Mata Pelajaran{" "}
                </span>
              </a>

              <a
                href="/kelas"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <i class="fa-solid fa-chalkboard-user"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Kelas
                </span>
              </a>

              <a
                href="/jenjang"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <i class="fa-solid fa-chart-simple"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Jenjang{" "}
                </span>
              </a>

              <a
                href="/jenisMataPelajaran"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <i class="fa-solid fa-book-open-reader"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Jenis Mata Pelajaran{" "}
                </span>
              </a>

              <a
                href="/guru"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <i class="fa-solid fa-user"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Guru{" "}
                </span>
              </a>

              <a
                href="/pendaftaransiswa"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <i class="fa-solid fa-file-pen"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Pendaftaran Siswa{" "}
                </span>
              </a>

              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <i class="fa-solid fa-school"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Pembagian Kelas{" "}
                </span>
              </a>

              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <i class="fa-solid fa-user-graduate"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Siswa{" "}
                </span>
              </a>

              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <i class="fa-solid fa-people-arrows"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Mutasi{" "}
                </span>
              </a>
            </nav>
          </details>

          <details class="group [&_summary::-webkit-details-marker]:hidden">
            <summary class="flex cursor-pointer items-center justify-between rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700">
              <div class="flex items-center gap-2">
                <i class="fa-solid fa-book"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Perpustakaan{" "}
                </span>
              </div>

              <span
                class={`shrink-0 transition duration-300 group-open:-rotate-180 ${
                  !open && "scale-0"
                }`}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fill-rule="evenodd"
                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                    clip-rule="evenodd"
                  />
                </svg>
              </span>
            </summary>

            <nav aria-label="Teams Nav" class="mt-2 flex flex-col px-4">
              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <i class="fa-solid fa-swatchbook"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Data Rak Buku{" "}
                </span>
              </a>

              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <i class="fa-sharp fa-solid fa-server"></i>
                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Data Kategori Buku{" "}
                </span>
              </a>

              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <i class="fa-sharp fa-solid fa-server"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Data Buku{" "}
                </span>
              </a>

              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <i class="fa-sharp fa-solid fa-server"></i>
                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  DAta Anggota{" "}
                </span>
              </a>

              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <i class="fa-regular fa-book-circle-arrow-right"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Peminjaman{" "}
                </span>
              </a>

              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
              <i class="fas fa-arrow-right"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  pengembalian{" "}
                </span>
              </a>

              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
               <i class="fas fa-sticky-note"></i>
                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Laporan{" "}
                </span>
              </a>
            </nav>
          </details>

          <details class="group [&_summary::-webkit-details-marker]:hidden">
            <summary class="flex cursor-pointer items-center justify-between rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700">
              <div class="flex items-center gap-2">
                <i class="fa-solid fa-marker"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Nilai{" "}
                </span>
              </div>

              <span
                class={`shrink-0 transition duration-300 group-open:-rotate-180} ${
                  !open && "scale-0"
                }`}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fill-rule="evenodd"
                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                    clip-rule="evenodd"
                  />
                </svg>
              </span>
            </summary>

            <nav aria-label="Account Nav" class="mt-2 flex flex-col px-4">
              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5 opacity-75"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  stroke-width="2"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    d="M10 6H5a2 2 0 00-2 2v9a2 2 0 002 2h14a2 2 0 002-2V8a2 2 0 00-2-2h-5m-4 0V5a2 2 0 114 0v1m-4 0a2 2 0 104 0m-5 8a2 2 0 100-4 2 2 0 000 4zm0 0c1.306 0 2.417.835 2.83 2M9 14a3.001 3.001 0 00-2.83 2M15 11h3m-3 4h2"
                  />
                </svg>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Input Nilai{" "}
                </span>
              </a>

              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5 opacity-75"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  stroke-width="2"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    d="M9 12l2 2 4-4m5.618-4.016A11.955 11.955 0 0112 2.944a11.955 11.955 0 01-8.618 3.04A12.02 12.02 0 003 9c0 5.591 3.824 10.29 9 11.622 5.176-1.332 9-6.03 9-11.622 0-1.042-.133-2.052-.382-3.016z"
                  />
                </svg>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Data Nilai{" "}
                </span>
              </a>

              <form action="/logout">
                <button
                  type="submit"
                  class="flex w-full items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    class="h-5 w-5 opacity-75"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    stroke-width="2"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1"
                    />
                  </svg>

                  <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                    {" "}
                    Cetak Raport{" "}
                  </span>
                </button>
              </form>
            </nav>
          </details>
          <details class="group [&_summary::-webkit-details-marker]:hidden">
            <summary class="flex cursor-pointer items-center justify-between rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700">
              <div class="flex items-center gap-2">
                <i class="fa-solid fa-dollar-sign"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Keuangan{" "}
                </span>
              </div>

              <span
                class={`shrink-0 transition duration-300 group-open:-rotate-180 ${
                  !open && "scale-0"
                }`}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fill-rule="evenodd"
                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                    clip-rule="evenodd"
                  />
                </svg>
              </span>
            </summary>

            <nav aria-label="Account Nav" class="mt-2 flex flex-col px-4">
              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5 opacity-75"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  stroke-width="2"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    d="M10 6H5a2 2 0 00-2 2v9a2 2 0 002 2h14a2 2 0 002-2V8a2 2 0 00-2-2h-5m-4 0V5a2 2 0 114 0v1m-4 0a2 2 0 104 0m-5 8a2 2 0 100-4 2 2 0 000 4zm0 0c1.306 0 2.417.835 2.83 2M9 14a3.001 3.001 0 00-2.83 2M15 11h3m-3 4h2"
                  />
                </svg>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Rencana Anggaran{" "}
                </span>
              </a>

              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5 opacity-75"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  stroke-width="2"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    d="M9 12l2 2 4-4m5.618-4.016A11.955 11.955 0 0112 2.944a11.955 11.955 0 01-8.618 3.04A12.02 12.02 0 003 9c0 5.591 3.824 10.29 9 11.622 5.176-1.332 9-6.03 9-11.622 0-1.042-.133-2.052-.382-3.016z"
                  />
                </svg>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Akun{" "}
                </span>
              </a>

              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5 opacity-75"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  stroke-width="2"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                  />
                </svg>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Input Data Masuk/Keluar{" "}
                </span>
              </a>

              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                <i class="fas fa-journal-whills"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Jurnal{" "}
                </span>
              </a>

              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
              <i class="fas fa-newspaper"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Laporan Jurnal{" "}
                </span>
              </a>
              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
                
                <i class="fas fa-book"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Laporan Buku Besar{" "}
                </span>
              </a>

              <a
                href="#"
                class="flex items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
              >
              <i class="fas fa-balance-scale"></i>

                <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                  {" "}
                  Laporan Neraca{" "}
                </span>
              </a>

              <form action="/logout">
                <button
                  type="submit"
                  class="flex w-full items-center gap-2 rounded-lg px-4 py-2 text-white hover:bg-gray-100 hover:text-gray-700"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    class="h-5 w-5 opacity-75"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    stroke-width="2"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1"
                    />
                  </svg>

                  <span class={`text-sm font-medium ${!open && "scale-0"}`}>
                    {" "}
                    Logout{" "}
                  </span>
                </button>
              </form>
            </nav>
          </details>
        </nav>
      </div>

      {/* <div class={`sticky inset-x-0 bottom-0 border-t border-gray-100 ${!open && "scale-0"}`}>
      <a
        href="#"
        class="flex items-center gap-2 bg-white p-4 hover:bg-gray-50"
      >
        <img
          alt="Man"
          src="https://images.unsplash.com/photo-1600486913747-55e5470d6f40?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80"
          class="h-10 w-10 rounded-full object-cover"
        />

        <div>
          <p class="text-xs">
            <strong class={`text-sm font-medium ${!open && "scale-0"}`}>
              Eric Frusciante
            </strong>

            <span> eric@frusciante.com </span>
          </p>
        </div>
      </a>
    </div> */}
    </div>
  );
}
