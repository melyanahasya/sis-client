import React from 'react';
import"../Css/Sidebar1.css";

export default function Navbar() {
  return (
    <div className='bg-blue-500 w-full h-[3rem] text-white flex justify-around gap-[8rem] md:gap-[77rem]'>
      <div className='bg-blue-600 px-[1.3rem] w-[4.7rem]'>
      <p className='mt-3 font-bold mr-[2rem]'>SIS</p>
      </div>
      <div className='flex gap-x-2'>
      <i class="fas fa-user-circle text-3xl mt-2 md:mt-2"></i>
      <p className='mt-3 md:mt-2'>Administrator</p>
      </div>
    </div>
  )
}
