import { Modal } from "react-bootstrap";
import React, { useState } from "react";

export default function () {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div className="px-5 absolute w-full">
      <div className="flex md:gap-[75%] gap-5 p-5">
        <h1 className="md:text-lg text-sm font-bold">Tahun Ajaran</h1>
        <a className="md:ml-11 md:text-sm text-xs text-blue-600" href="">
           Dashboard <span className="text-black">/ Tahun Ajaran</span>
        </a>
      </div>
      <hr className="h-1 bg-slate-500 mx-5" />
      <div className="flex flex-col shadow-2xl p-4">
        <div className="flex md:gap-[80%] gap-4">
          <h1 className="font-bold text-gray-500 md:text-lg md:w-40 w-48 text-sm">
            Data Tahun Ajaran
          </h1>
          <button
            data-modal-target="authentication-modal"
            data-modal-toggle="authentication-modal"
            className="md:text-base w-20 md:w-24 float-right text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-sm text-xs md:h-9 h-6 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            type="submit"
            onClick={handleShow}
          >
            <i class="fas fa-plus"></i> Tambah
          </button>
        </div>

        <nav className="navbar navbar-light bg-light">
        <form className="form-inlined-flex float-right py-1">
              <button
                className="btn btn-outline-success text-xs pl-2 my-2 my-sm-0"
                type="submit"
              >
                Search :
              </button>
              <input
                className="form-control border ml-2 md:w-40 w-24 md:h-6 h-5 mr-sm-2"
                type="search"
                aria-label="Search"
              />
            </form>
        </nav>
        <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 inline-block min-w-full sm:px-6 lg:px-8">
            <div className="overflow-hidden">
              <table className="min-w-full border text-center">
                <thead className="border-b">
                  <tr>
                    <th
                      scope="col"
                      className="md:text-sm text-xs font-medium text-gray-900 w-4 md:py-4 py-2border-r"
                    >
                      No
                    </th>
                    <th
                      scope="col"
                      className="md:text-sm text-xs font-medium text-gray-900 px-3 md:py-4 py-2 border-r"
                    >
                      Kode Tahun Ajaran
                    </th>
                    <th
                      scope="col"
                      className="md:text-sm text-xs font-medium text-gray-900 px-3 md:py-4 py-2 border-r"
                    >
                      Nama
                    </th>
                    <th
                      scope="col"
                      className="md:text-sm text-xs font-medium text-gray-900 px-3 md:py-4 py-2 border-r"
                    >
                      Awal Periode
                    </th>
                    <th
                      scope="col"
                      className="md:text-sm text-xs font-medium text-gray-900 px-3 md:py-4 py-2 border-r"
                    >
                      Akhir Periode
                    </th>
                    <th
                      scope="col"
                      className="md:text-sm text-xs font-medium text-gray-900 px-3 md:py-4 py-2 border-r"
                    >
                      Keterangan
                    </th>
                    <th
                      scope="col"
                      className="md:text-sm text-xs font-medium text-gray-900 px-3 md:py-4 py-2 border-r"
                    >
                      Status
                    </th>
                    <th
                      scope="col"
                      className="md:text-sm text-xs font-medium text-gray-900 px-3 md:py-4 py-2 border-r"
                    >
                      Aksi
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-gray-100">
                  <tr className="border-b">
                    <td className="px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 border-r">
                      1
                    </td>
                    <td className="md:text-sm text-xs text-gray-900 font-light px-6 py-4 whitespace-nowrap border-r">
                      TA2018/2019
                    </td>
                    <td className="md:text-sm text-xs text-gray-900 font-light px-6 py-4 whitespace-nowrap border-r">
                      Melyana
                    </td>
                    <td className="md:text-sm text-xs text-gray-900 font-light px-6 py-4 whitespace-nowrap border-r">
                      17 Juli 2022
                    </td>
                    <td className="md:text-sm text-xs text-gray-900 font-light px-6 py-4 whitespace-nowrap border-r">
                      30 Juni 2023
                    </td>
                    <td className="md:text-sm text-xs text-gray-900 font-light px-6 py-4 whitespace-nowrap border-r">
                      -
                    </td>
                    <td className="md:text-sm text-xs text-gray-900 font-light px-6 py-4 whitespace-nowrap border-r">
                      Aktif
                    </td>
                    <td className="md:text-sm text-xs md:ml-[14%] text-gray-900 font-light px-6 py-4 whitespace-nowrap flex gap-3 border-r">
                      <p className="border w-14 bg-blue">
                        <i className="fas fa-edit"></i> edit
                      </p>
                      <div className="border w-16">
                        <p classNameName=" bg-blue">
                          <i className="fas fa-trash-alt"></i> hapus
                        </p>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>

              <div className="flex md:gap-[70%] gap-[60%]">
                <p className="md:text-base text-xs w-64 text-gray-600 py-3">
                Showing 1 to 1 of 1 entries
                </p>

                <nav aria-label="Page navigation example">
                  <ul className="md:h-8 h-5 mt-4 inline-flex items-center px-2 py-1 text-sm font-medium text-gray-700 hover:text-gray-500 bg-slate-100 hover:bg-slate-50 border-gray-500 border">
                    <li>
                      <a className="pointer-events-none relative block rounded bg-transparent py-1.5 px-3 text-sm text-gray-700 transition-all duration-300">
                        Previous
                      </a>
                    </li>
                    <li aria-current="page">
                      <a
                        className="relative block rounded bg-primary-100 md:py-1.5 md:px-3 text-sm font-medium text-primary-700 transition-all duration-300"
                        href="#!"
                      >
                        1
                        <span className="absolute -m-px h-px w-px overflow-hidden whitespace-nowrap border-0 p-0 [clip:rect(0,0,0,0)]">
                          (current)
                        </span>
                      </a>
                    </li>
                    <li>
                      <a
                        className="relative block rounded bg-transparent py-1.5 px-3 text-sm text-gray-700 transition-all duration-300"
                        href="#!"
                      >
                        Next
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>

              <Modal
                show={show}
                onHide={handleClose}
                id="authentication-modal"
                tabindex="-1"
                aria-hidden="true"
                className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
              >
                <div className="relative w-full h-full max-w-md md:h-auto">
                  <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
                    <button
                      type="button"
                      className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                      data-modal-hide="authentication-modal"
                      onClick={handleClose}
                    >
                      <svg
                        aria-hidden="true"
                        className="w-5 h-5"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fill-rule="evenodd"
                          d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                          clip-rule="evenodd"
                        ></path>
                      </svg>
                      <span className="sr-only">Close modal</span>
                    </button>
                    <div className="px-6 py-6 lg:px-8">
                      <h3 className="mb-4 md:text-xl text-base font-medium text-gray-900 dark:text-white">
                        Tambahkan
                      </h3>
                      <form
                        className="space-y-3"
                        // onSubmit={add}
                      >
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Kode Tahun Ajaran
                          </label>
                          <input
                            placeholder="Kode Tahun Ajaran"
                            // onChange={(e) => setKodeTahunAjaran(e.target.value)}
                            // value={kodeTahunAjaran}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Nama
                          </label>
                          <input
                            placeholder="Nama"
                            // onChange={(e) => setNama(e.target.value)}
                            // value={nama}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Awal Periode
                          </label>
                          <input
                            placeholder="Awal Periode"
                            // onChange={(e) => setAwalPeriode(e.target.value)}
                            // value={awalPeriode}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Akhir periode
                          </label>
                          <input
                            placeholder="Akhir periode"
                            // onChange={(e) => setAkhiePeriode(e.target.value)}
                            // value={akhirPeriode}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Keterangan
                          </label>
                          <input
                            placeholder="Keterangan"
                            // onChange={(e) => setKeterangan(e.target.value)}
                            // value={keterangan}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                            Status
                          </label>
                          <input
                            placeholder="Status"
                            // onChange={(e) => setStatus(e.target.value)}
                            // value={status}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                            required
                          />
                        </div>
                        <button
                          onClick={handleClose}
                          type="submit"
                          className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                        >
                          Tambah
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
              </Modal>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
